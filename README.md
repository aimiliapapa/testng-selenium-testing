**How to produce the custom html reports from custom Listenner:**

Run the testng.xml file in: src\main\java\ .The complete test suite will run and the output results will be saved inside the test-output\Emily's Custom Reports\\



---------------------------------------------------------------------------------------------------------------------------------------------------

**How to view the reporter logs:**

The reporter logs are very helpful to us because we can see every step's custom message on screen. This way, we are certain that all of our steps were run and where our test may fail.\
Reporter logs can be found in: test-output\old\Emily's Custom Reports\index and then click reporter output.
