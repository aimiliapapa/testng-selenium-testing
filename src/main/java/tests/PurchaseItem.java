package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;


public class PurchaseItem {

    WebDriver driver;
    public String url = "https://www.saucedemo.com/";
    public String userName = "standard_user";
    public String password = "secret_sauce";
    public String productPageTitle = "Products";
    public String checkoutPageTitle = "Checkout: Your Information";
    public String firstName = "Emily";
    public String lastName = "Papadourou";
    public String postalCode = "10443";
    public String overviewPageTitle = "Checkout: Overview";
    public String completePageTitle = "Checkout: Complete!";

    By usernameField = By.id("user-name");
    By passwordField = By.id("password");
    By signInButton = By.id("login-button");
    By item = By.id("add-to-cart-test.allthethings()-t-shirt-(red)");
    By itemTitle = By.className("inventory_item_name");
    By itemCartCounter = By.xpath("//*[@id=\"shopping_cart_container\"]/a/span");
    By cartIcon = By.className("shopping_cart_link");
    By checkoutButton = By.id("checkout");
    By firstNameField = By.id("first-name");
    By lastNameField = By.id("last-name");
    By postalCodeField = By.id("postal-code");
    By continueButton = By.id("continue");
    By finishButton = By.id("finish");
    By completeOrderMsg = By.className("complete-header");


    //Log in user before every test case
    @BeforeMethod(alwaysRun=true)
    public void loginUser()
    {
        //Initiate WebDriver
//      System.setProperty("webdriver.firefox.driver", "user.dir\\geckodriver.exe");
        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        Reporter.log("Browser is Maximized");
        driver.get(url);
        Reporter.log("Website URL is opened");
        //Clear field and enter UserName
        driver.findElement(usernameField).clear();
        driver.findElement(usernameField).sendKeys(userName);
        Reporter.log("Username is entered.");
        //Clear field and enter password
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        Reporter.log("Password is entered.");
        //Click sign in Button
        driver.findElement(signInButton).click();
        Reporter.log("Sign in button is clicked.");
        //Get Page Title
        String actualTitle = driver.findElement(By.className("title")).getText();
        String expectedTitle = productPageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(actualTitle, expectedTitle);
        Reporter.log("User is logged in!");



    }

   @Test
    public void purchaseItem(){
        //Add Item to Cart
        driver.findElement(item).click();
        Reporter.log("Item is clicked.");
        //Get Actual Number of Items in  Cart
        String actualItem = driver.findElement(itemCartCounter).getText();
        //Get Expected Number of Items in Cart
        String expectedItem = "1";
        //Assert that Expected and Actual Item Number matches
        Assert.assertEquals(expectedItem, actualItem);
        Reporter.log("Items in cart and items in cart icon number match.");
        //Click cart icon
        driver.findElement(cartIcon).click();
        Reporter.log("Cart icon is clicked");
        //Wait for item to appear in cart
        WebDriverWait wait = new WebDriverWait(driver,  Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOfElementLocated(itemTitle));
        Reporter.log("Item exists in cart.");
        //Click Checkout Button
        driver.findElement(checkoutButton).click();
        //Get Page Title
        String ActualTitle = driver.findElement(By.className("title")).getText();
        String ExpectedTitle = checkoutPageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(ActualTitle, ExpectedTitle);
        Reporter.log("Page Title is correct.");
        //Enter FirstName
        driver.findElement(firstNameField).sendKeys(firstName);
        Reporter.log("First Name is Entered");
        //Enter Last Name
        driver.findElement(lastNameField).sendKeys(lastName);
        Reporter.log("Last Name is Entered");
        //Enter Postal Code
        driver.findElement(postalCodeField).sendKeys(postalCode);
        Reporter.log("Postal Code is entered");
        //Click Continue Button
        driver.findElement(continueButton).click();
        Reporter.log("Continue Button is clicked");
        //Get Page Title
        String actualTitle = driver.findElement(By.className("title")).getText();
        String expectedTitle = overviewPageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(actualTitle, expectedTitle);
        Reporter.log("Page Title is correct.");
        //Assert that quantity in checkout is same as item(s)
        //Get Actual Number of Items in  Cart
        String expectedQuantity = driver.findElement(itemCartCounter).getText();
        //Get Expected Number of Items in Cart
        String actualQuantity = driver.findElement(By.className("cart_quantity")).getText();
        //Assert that Expected and Actual Item Number matches
        Assert.assertEquals(expectedQuantity, actualQuantity);
        Reporter.log("Items quantity is correct");
        //Click Finish Button
        driver.findElement(finishButton).click();
        Reporter.log("Your order has been completed :D");
        //Assert that Complete Page Appears
        String actual = driver.findElement(By.className("title")).getText();
        String expected = completePageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(actual, expected);
        Reporter.log("Page Title is correct.");
        //Assert that complete message apppears
        WebDriverWait w = new WebDriverWait(driver,  Duration.ofSeconds(5));
        w.until(ExpectedConditions.visibilityOfElementLocated(completeOrderMsg));
        Reporter.log("Completed Order Message appeared.");


    }

    //Close browser after every test case
    @AfterMethod(alwaysRun=true)
    public void tearDown() {
        driver.quit();
    }

}
