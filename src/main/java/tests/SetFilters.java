package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class SetFilters {

    WebDriver driver;
    public String url = "https://www.saucedemo.com/";
    public String userName = "standard_user";
    public String password = "secret_sauce";
    public String productPageTitle = "Products";
    By usernameField = By.id("user-name");
    By passwordField = By.id("password");
    By signInButton = By.id("login-button");
    By filterIcon = By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select");
    By ZtoAFIlter = By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[2]");
    By appliedFilter = By.className("active_option");


    //Log in user before every test case
    @BeforeMethod(alwaysRun=true)
    public void loginUser()
    {
        //Initiate WebDriver
//      System.setProperty("webdriver.firefox.driver", "user.dir\\geckodriver.exe");
        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        Reporter.log("Browser is Maximized");
        driver.get(url);
        Reporter.log("Website URL is opened");
        //Clear field and enter UserName
        driver.findElement(usernameField).clear();
        driver.findElement(usernameField).sendKeys(userName);
        Reporter.log("Username is entered.");
        //Clear field and enter password
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        Reporter.log("Password is entered.");
        //Click sign in Button
        driver.findElement(signInButton).click();
        Reporter.log("Sign in button is clicked.");
        //Get Page Title
        String actualTitle = driver.findElement(By.className("title")).getText();
        String expectedTitle = productPageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(actualTitle, expectedTitle);
        Reporter.log("User is logged in!");
    }

    @Test
    public void setFilters(){
        //Wait For Element to be visible
        WebDriverWait wait = new WebDriverWait(driver,  Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(filterIcon));
        Reporter.log("Filters Icon is visible");
        //Click Filter Button
        driver.findElement(filterIcon).click();
        Reporter.log("Filter Icon is clicked.");
        //Click Filter Items From Z to A
        driver.findElement(ZtoAFIlter).click();
        Reporter.log("Filter is set.");
        //Get Clicked Choice(Actual)
        String actualChoice = driver.findElement(ZtoAFIlter).getText().toUpperCase();
        //Get Existing Choice(Expected)
        String expectedChoice = driver.findElement(appliedFilter).getText();
        //Assert that Expected and Actual Choice matches
        Assert.assertEquals(expectedChoice, actualChoice);
        Reporter.log("Set filter and applied filter match.");

    }

    //Close browser after every test case
    @AfterMethod(alwaysRun=true)
    public void tearDown() {
        driver.quit();
    }

}
