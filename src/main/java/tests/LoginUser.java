package tests;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class LoginUser {
    WebDriver driver;
    public String url = "https://www.saucedemo.com/";
    public String userName = "standard_user";
    public String password = "secret_sauce";
    public String pageTitle = "Swag Labs";
    public String productPageTitle = "Products";
    By usernameField = By.id("user-name");
    By passwordField = By.id("password");
    By signInButton = By.id("login-button");
    By errorMessage = By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]");


    //Open and maximize browser before every test case
    @BeforeMethod(alwaysRun=true)
    public void testSetup()
    {
        //Initiate WebDriver
//      System.setProperty("webdriver.firefox.driver", "user.dir\\geckodriver.exe");
        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        //Maximize Browser window
        driver.manage().window().maximize();
        Reporter.log("Browser is Maximized.");
        driver.get(url);
        Reporter.log("Website URL is opened.");

    }

    @Test
    public void validUser(){
        //Get Window Title
        String ActualTitle = driver.getTitle();
        String ExpectedTitle = pageTitle;
        //Assert that Window Title is correct
        Assert.assertEquals(ActualTitle, ExpectedTitle);
        Reporter.log("Window Title is correct.");
        //Clear field and enter UserName
        driver.findElement(usernameField).clear();
        driver.findElement(usernameField).sendKeys(userName);
        Reporter.log("Username is entered.");
        //Clear field and enter password
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        Reporter.log("Password is entered.");
        //Click sign in Button
        driver.findElement(signInButton).click();
        Reporter.log("Sign in button is clicked.");
        //Get Page Title
        String actualTitle = driver.findElement(By.className("title")).getText();
        String expectedTitle = productPageTitle.toUpperCase();
        //Assert that Page Title is correct
        Assert.assertEquals(actualTitle, expectedTitle);
        Reporter.log("User is logged in!");

    }

    @Test
    public void emptyUsername(){
        //Clear field and leave UserName blank
        driver.findElement(usernameField).clear();
        Reporter.log("Username is empty.");
        //Clear field and enter password
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        Reporter.log("Password is entered.");
        //Click sign in Button
        driver.findElement(signInButton).click();
        Reporter.log("Sign in Button is clicked.");
        //Get Error Message Text
        String ActualError = driver.findElement(errorMessage).getText();
        //Give Expected Error Message Text
        String expectedEmptyUserError = "Epic sadface: Username is required";
        //Assert That Error Message is the Expected
        Assert.assertEquals(ActualError, expectedEmptyUserError);
        Reporter.log("Correct error message appeared.");

    }


    @Test
    public void invalidUsername(){
        //Clear field and enter invalid Username
        driver.findElement(usernameField).clear();
        driver.findElement(usernameField).sendKeys("wrongUser");
        Reporter.log("Invalid UserName is entered.");
        //Clear field and enter password
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        Reporter.log("Password is entered.");
        //Click sign in Button
        driver.findElement(signInButton).click();
        Reporter.log("Sign in Button is clicked.");
        //Get Error Message Text
        String ActualError = driver.findElement(errorMessage).getText();
        //Give Expected Error Message Text
        String expectedWrongUserError = "Epic sadface: Username and password do not match any user in this service";
        //Assert That Error Message is the Expected
        Assert.assertEquals(ActualError, expectedWrongUserError);
        Reporter.log("Correct error message appeared.");

    }

    //Close browser after every test case
    @AfterMethod(alwaysRun=true)
    public void tearDown() {
        driver.quit();
    }

}
